package cn.itcast.bos.web.action;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.Session;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Controller;

import cn.itcast.bos.domain.constant.Constants;
import cn.itcast.crm.domain.Customer;
import cn.itcast.sms.utils.MailUtils;
@Namespace("/")
@ParentPackage("json-default")
@Controller
@Scope("prototype")
public class CustomerAction extends BaseAction<Customer> {
	@Autowired
	@Qualifier("jmsQueueTemplate")
	private JmsTemplate jmsTemplate;
	
	
	@Action(value="customer_sendSms")
	public String sendSms() throws IOException{
		String randomCode=RandomStringUtils.randomNumeric(4);
		System.out.println("生成手机的验证码为"+randomCode);
		//将短信验证码存储到session当中
		ServletActionContext.getRequest().getSession().setAttribute(model.getTelephone(), randomCode);
		final String msg="尊敬的快递员您好，本次获取的验证码为："
				 + randomCode + ",服务电话：4006184000";
		
		jmsTemplate.send("bos_sms",new MessageCreator() {
			
			@Override
			public Message createMessage(Session session) throws JMSException {
				MapMessage mapMessage = session.createMapMessage();
				mapMessage.setString("telephone", model.getTelephone());
				mapMessage.setString("msg", msg);
				return mapMessage;
			}
		});
		
		return NONE;
		
		
//		String result=SmsUtils.sendSmsByHTTP(model.getTelephone(), msg);
		/*String result="000/xxx";
		if(result.startsWith("000")){
			return NONE;
		}else{
			throw new RuntimeException("短信发送失败,信息码为"+result);
		}*/
	}
	
	private String checkcode;
	
	public void setCheckcode(String checkcode) {
		this.checkcode = checkcode;
	}
	@Autowired
	private RedisTemplate<String, String> redisTemplate;
	
	@Action(value="customer_regist",
			results={
				@Result(name="success",location="./signup-success.html",type="redirect"),
				@Result(name="input",location="./signup.html",type="redirect")
			})
	public String regist(){
		String sessionCode=
				(String) ServletActionContext.getRequest().getSession().getAttribute(model.getTelephone());
		if(checkcode.equalsIgnoreCase(sessionCode)){
			System.out.println("客户注册成功");
			WebClient.create("http://localhost:9002/crm_management/services/customerService/customer")
			         .type(MediaType.APPLICATION_JSON).post(model);
			
			
			//发送一封激活邮件
			//生成激活码
			String activeCode=RandomStringUtils.randomNumeric(32);
			//将激活码保存到redis当中，设置24小时失效
			redisTemplate.opsForValue().set(model.getTelephone(), activeCode, 24,TimeUnit.HOURS);
			//调用MailUtils发送激活邮件
			String content="尊敬的客户您好，请于24小时内，进行邮箱账户的绑定，点击下面地址完成绑定<a href='"+
			MailUtils.activeUrl+"?telephone="+model.getTelephone()+"&activeCode="+activeCode+"'>速运快递邮箱绑定地址</a>";
			
			MailUtils.sendMail("速运快递激活邮件", content, "zhouleizuiniu@163.com");
			return SUCCESS;
		}else{
			System.out.println("短信验证码发生错误");
			return INPUT;
		}
	}
	
	private String activeCode;
	
	
	public void setActiveCode(String activeCode) {
		this.activeCode = activeCode;
	}


	@Action(value="customer_activeMail")
	public String activeMail() throws IOException{
		ServletActionContext.getResponse().setContentType("text/html;charset=utf-8");
		String activeCodeRedis=redisTemplate.opsForValue().get(model.getTelephone());
		if(activeCodeRedis==null||!activeCodeRedis.equals(activeCode)){
			//激活码无效
			ServletActionContext.getResponse().getWriter().println("激活码无效，请重新登录帐号进行绑定");
		}else{
			//激活码有效
			//防止重复绑定
			Customer customer= WebClient.create("http://localhost:9002/crm_management/services/customerService/customer/telephone/"+model.getTelephone())
	         .accept(MediaType.APPLICATION_JSON).get(Customer.class);
			if(customer.getType()==null||customer.getType()!=1){
				//没有绑定，进行绑定
				WebClient.create("http://localhost:9002/crm_management/services/customerService/customer/updatetype/"+model.getTelephone())
				         .type(MediaType.APPLICATION_JSON).get();
				ServletActionContext.getResponse().getWriter().println("激活码无效，请重新登录帐号进行绑定");
			}else{
				ServletActionContext.getResponse().getWriter().println("已经绑定，无须重复绑定");
			}
			
			redisTemplate.delete(model.getTelephone());
		}
		
		return NONE;
	}
	@Action(value="customer_login",results={
			@Result(name="login",location="login.html",type="redirect"),
			@Result(name="success",location="index.html#/myhome",type="redirect")
	})
	public String login(){
		Customer customer = WebClient.create(Constants.CRM_MANAGEMENT_URL+"/services/customerService/customer/login?telephone="+model.getTelephone()+"&password="+model.getPassword()).accept(MediaType.APPLICATION_JSON).get(Customer.class);
		if(customer==null){
			return "login";
		}else{
			ServletActionContext.getRequest().getSession().setAttribute("customer", customer);
			return SUCCESS;
		}
	}
	
}
