package cn.itcast.bos.service.base.impl;

import java.util.List;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.itcast.bos.dao.base.CourierRepository;
import cn.itcast.bos.domain.base.Courier;
import cn.itcast.bos.service.base.CourierService;
@Service
@Transactional
public class CourierServiceImpl implements CourierService {
	@Autowired
	private CourierRepository c;
	

	@Override
	@RequiresPermissions("courier_add")
	public void save(Courier courier) {
		c.save(courier);
		
	}


	@Override
	public Page<Courier> findPageData(Specification<Courier> specification, Pageable pageable) {
		// TODO Auto-generated method stub
		return c.findAll(specification, pageable);
	}


	@Override
	public void delBatch(String[] s) {
		for (String str : s) {
			Integer id=Integer.parseInt(str);
			c.updateDelBatch(id);
		}
		
	}


	@Override
	public void restore(String[] s) {
		for (String str : s) {
			Integer id=Integer.parseInt(str);
			c.updateRestore(id);
		}
		
	}


	@Override
	public List<Courier> findnoassociation() {
		// TODO Auto-generated method stub
		Specification<Courier> specification=new Specification<Courier>() {

			@Override
			public Predicate toPredicate(Root<Courier> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				Predicate p=cb.isEmpty(root.get("fixedAreas").as(Set.class));
				return p;
			}
		};
		return c.findAll(specification);
	}


	


	

}
