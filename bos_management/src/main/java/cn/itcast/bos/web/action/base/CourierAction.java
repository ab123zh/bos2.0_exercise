package cn.itcast.bos.web.action.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.itcast.bos.domain.base.Courier;
import cn.itcast.bos.service.base.CourierService;
@Controller
@Scope("prototype")
@ParentPackage("json-default")
@Namespace("/")
public class CourierAction extends ActionSupport implements ModelDriven<Courier>{
	private Courier courier=new Courier();
	@Override
	public Courier getModel() {
		// TODO Auto-generated method stub
		return courier;
	}
	@Autowired
	private CourierService courierService;
	
	@Action(value="courier_save",results={@Result(name="success",location="./pages/base/courier.html",type="redirect")})
	public String save(){
		System.out.println("添加成功");
		courierService.save(courier);
		return SUCCESS;
	}
	
	private int page;
	private int rows;
	public void setPage(int page) {
		this.page = page;
	}
	public void setRows(int rows) {
		this.rows = rows;
	}
	@Action(value="courier_pageQuery",results={@Result(name="success",type="json")})
	public String pageQuery(){
		Pageable pageable=new PageRequest(page-1, rows);
		// 封装条件查询对象 Specification
		Specification<Courier> specification=new Specification<Courier>() {
			// Root 用于获取属性字段，CriteriaQuery可以用于简单条件查询，CriteriaBuilder 用于构造复杂条件查询
			@Override
			public Predicate toPredicate(Root<Courier> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> list=new ArrayList<>();
				if(StringUtils.isNotBlank(courier.getType())){
					//root.get("type")---type=?
					Predicate p1=cb.equal(root.get("type").as(String.class),courier.getType() );
					list.add(p1);
				}
				
				if(StringUtils.isNotBlank(courier.getCompany())){
					Predicate p2=cb.like(root.get("company").as(String.class), "%"+courier.getCompany()+"%");
					list.add(p2);
				}
				
				if(StringUtils.isNotBlank(courier.getCourierNum())){
					Predicate p3=cb.equal(root.get("courierNum").as(String.class), courier.getCourierNum());
					list.add(p3);
				}
				//多表查询(查询当前对象,关联对象，对应数据表)
				//使用courier,关联standard
				//隐士内连接-----from courie.standard,下面再来from courie.standard,name=?
				Join<Object, Object> standRoot=root.join("standard",JoinType.INNER);
				if(courier.getStandard()!=null&&StringUtils.isNotBlank(courier.getStandard().getName())){
					Predicate p4=cb.like(standRoot.get("name").as(String.class),"%"+courier.getStandard().getName()+"%" );
					list.add(p4);
				}
				
				
				return cb.and(list.toArray(new Predicate[0]));
			}
		};
		
		
		Page<Courier> pageData=courierService.findPageData(specification,pageable);
		Map<String , Object> map=new HashMap<>();
		map.put("total", pageData.getTotalElements());
		map.put("rows", pageData.getContent());
		ActionContext.getContext().getValueStack().push(map);
		return SUCCESS;
	}
	@Action(value="courier_delBatch",results={@Result(name="success",location="./pages/base/courier.html",type="redirect")})
	public String delBatch(){
		String ids=ServletActionContext.getRequest().getParameter("ids");
		String[] s = ids.split(",");
		courierService.delBatch(s);
		return SUCCESS;
	}

	@Action(value="courier_restore",results={@Result(name="success",location="./pages/base/courier.html",type="redirect")})
	public String restore(){
		String ids=ServletActionContext.getRequest().getParameter("ids");
		String[] s = ids.split(",");
		courierService.restore(s);
		return SUCCESS;
	}
	
	@Action(value="courier_findnoassociation",results={@Result(name="success",type="json")})
	public String  findnoassociation (){
		List<Courier> list=courierService.findnoassociation();
		ActionContext.getContext().getValueStack().push(list);
		return SUCCESS;
	}
	
	
}
